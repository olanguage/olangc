package main

import (
	"github.com/urfave/cli"
	"os"
	"sort"
  "gitlab.com/olanguage/olangc/compiler"
  "fmt"
)


func main(){
	app := &cli.App{
		Name: "olangc",
		Usage: "Olang Compiler",
		Version: "1.0",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "input, i",
				Usage: "Input File",
			},
		},
		Commands: []*cli.Command{
			{
				Name:    "compile",
				Aliases: []string{"c"},
				Usage:   "Compile Olang Code ",
				Action:  func(c *cli.Context) error {
					input := c.String("input")
					
					if len(input) > 0 {
						filename := input
						c := compiler.New(filename)
						
						fmt.Printf("Compiling... %s\n", c.Filename)
					}else{
						if c.NArg() > 0 {
							name := c.Args()[0]
							c := compiler.New(name)
							
							fmt.Printf("Compiling... %s\n", c.Filename)
							
							p := c.ParseFile()
							p.Compile()
						}else{
							fmt.Printf("Please enter filename to compile!\n")
						}
					}
					return nil
				},
			},
		},
	}

	sort.Sort(cli.CommandsByName(app.Commands))

	app.Run(os.Args)
}