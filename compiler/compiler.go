package compiler

import (
	"gitlab.com/olanguage/olang/lexer"
	"gitlab.com/olanguage/olang/parser"
	"io/ioutil"
	"strings"
	"regexp"
	"os"
	"path/filepath"
)

type File struct{
    Filename string
    Source string
}

type Result struct{
    Error bool
    Message string
    RawData string
    File *File 
}


type Compiler interface{
    ParseFile() Result
    Compile() Result
}


func New(input string) *File{
	extension := filepath.Ext(input)
	name := input[0:len(input)-len(extension)]
    return &File{
        Filename: input,
        Source: name+".binary.go",
    }
}

func (f *File) ParseFile() *Result{
    res := &Result{}
    
    res.File = f
    res.RawData = ""
    
	content, err := ioutil.ReadFile(f.Filename)
	if err != nil {
		res.Error = true
	}
	
	contstr := string(content)
	
	if len(contstr) > 0 {
		var re = regexp.MustCompile(`(?m)\#[\s\S]*?.*|\#\![\s\S]*?.*|\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$`)
		// \#[\s\S]*?.*|\/\*[\s\S]*?\*\/|([^:]|^)\/\/.*$
		
		var rel = regexp.MustCompile(`(?mi)(?:load)+\s\S+`)
		var loadRemove = regexp.MustCompile(`(?mi)(?:load)\W`)
		
		loads := rel.FindAllString(contstr, -2)
		
		loaded := ""
		for _,load := range loads{
			load = loadRemove.ReplaceAllString(load, "$1W")
			load = strings.Replace(load, "\"", "", -2)
			curDir, _ := os.Getwd()
			loadedContent, err := ioutil.ReadFile(curDir+"/"+load)
			if err != nil {
				res.Error = true
			}
			loaded += string(loadedContent)
		}
		
		contstr = rel.ReplaceAllString(contstr, loaded)
		contstr = re.ReplaceAllString(contstr, "$1W")
		contstr = strings.Replace(contstr, "\n", " ", -2)
		contstr = strings.Replace(contstr, "\t", " ", -2)
		contstr = strings.Replace(contstr, "  ", " ", -2)
		

		l := lexer.New(contstr)
		p := parser.New(l)
		
		program := p.ParseProgram()
		
		res.RawData += program.Code()
	}
    
    return res
}


func (r *Result) Compile() *Result{
	f, _ := os.Create(r.File.Source)
	defer f.Close()
	f.WriteString("package main\n\nfunc main(){\n\t"+r.RawData+"\n}\n")
	f.Sync()
    return r
}